# Copyright 2018-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV="v${PV}"

require github [ user=KDAB release=v${PV} suffix=tar.gz ] cmake
require gtk-icon-cache

export_exlib_phases src_prepare

SUMMARY="The Linux perf GUI for performance analysis"
DESCRIPTION="
This project is a KDAB R&D effort to create a standalone GUI for performance
data. As the first goal, we want to provide a UI like KCachegrind around Linux
perf. Looking ahead, we intend to support various other performance data
formats under this umbrella."

HOMEPAGE+=" https://www.kdab.com/"

# There are also licences for commercial usage, see LICENSE{,.US}.txt
LICENCES="GPL-2.0"
SLOT="0"
MYOPTIONS="
    call-graph [[ description = [ Call graph in the caller/callee tab ] ]]
    frequency-page [[ description = [ Adds an additional tab with frequency data ] ]]
    zstd [[ description = [ Transparent support for reading zstd compressed perf data files ] ]]
"

QT_MIN_VER="6.4"

DEPENDENCIES="
    build:
        kde-frameworks/extra-cmake-modules[>=1.0.0]
    build+run:
        dev-libs/kddockwidgets[providers:qt6]
        dev-util/elfutils[debuginfod]
        kde-frameworks/karchive:6
        kde-frameworks/kconfigwidgets:6
        kde-frameworks/kcoreaddons:6
        kde-frameworks/ki18n:6
        kde-frameworks/kiconthemes:6
        kde-frameworks/kio:6
        kde-frameworks/kitemmodels:6
        kde-frameworks/kitemviews:6
        kde-frameworks/knotifications:6
        kde-frameworks/kparts:6
        kde-frameworks/kwindowsystem:6
        kde-frameworks/solid:6
        kde-frameworks/syntax-highlighting:6
        kde-frameworks/threadweaver:6
        x11-libs/qtbase:6[>=${QT_MIN_VER}]
        x11-libs/qtsvg:6[>=${QT_MIN_VER}]
        call-graph? ( media-gfx/kgraphviewer[>=2.5.0] )
        frequency-page? ( dev-libs/qcustomplot[providers:qt6] )
        zstd? ( app-arch/zstd )
    run:
        sys-analyzer/perf
        sys-devel/binutils   [[ note = [ objdump ] ]]
    suggestion:
        dev-rust/rustc-demangle [[
            description = [ Demangling of Rust symbols ]
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DAPPIMAGE_BUILD:BOOL=FALSE
    # Qt5 would be boring, wouldn't it?
    -DQT6_BUILD:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'call-graph KGraphViewerPart'
    'frequency-page QCustomPlot'
    Zstd
)

hotspot_src_prepare() {
    cmake_src_prepare


    edo sed \
        -e "/::disassemble(/s/\"objdump\"/\"$(exhost --tool-prefix)objdump\"/" \
        -i tests/modeltests/tst_disassemblyoutput.cpp \
        -i tests/modeltests/tst_models.cpp
}

CMAKE_SRC_TEST_PARAMS+=(
    # Skip a test which wants to run perf, mount kernel modules, etc, which
    # isn't really feasible in our sandbox build env.
    # Not sure tst_models fails
    -E '(tst_perfparser|tst_models)'
)

