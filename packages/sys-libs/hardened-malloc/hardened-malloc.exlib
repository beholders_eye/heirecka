# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN="${PN/-/_}"

require github [ user=GrapheneOS ]

export_exlib_phases src_compile src_test src_install

SUMMARY="Hardened allocator designed for modern systems"
DESCRIPTION="
It has integration into Android's Bionic libc and can be used externally with
musl and glibc as a dynamic library for use on other Linux-based platforms. It
will gain more portability/integration over time."

HOMEPAGE+=" https://grapheneos.org/"

LICENCES="MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-lang/clang:*   [[ note = [ clang-tidy ] ]]
        dev-lang/python:*[>=3]
"

hardened-malloc_src_compile() {
    emake VARIANT=light
    emake VARIANT=default

}

hardened-malloc_src_test() {
    # The check target pulls in clang-tidy, which hinges on the used clang
    # version.
    emake test
}

hardened-malloc_src_install() {
    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)/lib
    dolib.so out/libhardened_malloc.so
    dolib.so out-light/libhardened_malloc-light.so

    emagicdocs
}

