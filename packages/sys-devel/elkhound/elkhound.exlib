# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV="$(ever replace_all -)"

require github [ user=WeiDUorg ] cmake

export_exlib_phases src_install

SUMMARY="A parser generator capable of emitting a GLR parser in OCaml"
DESCRIPTION="
It is similar to Bison. The parsers it generates use the Generalized LR (GLR)
parsing algorithm. GLR works with any context-free grammar, whereas LR parsers
(such as Bison) require grammars to be LALR(1).
Parsing with arbitrary context-free grammars has two key advantages:
(1) unbounded lookahead, and (2) support for ambiguous grammars. Unbounded
lookahead is achieved by allowing multiple potential parses to coexist for as
long as necessary. Similarly, ambiguity is handled by letting potential parses
be coalesced, with special action taken to handle the ambiguous region of the
input. In general, by using the GLR algorithm, Elkhound avoids the difficulty
of trying to make a language grammar fit the LALR(1) restrictions."

HOMEPAGE+=" https://scottmcpeak.com/elkhound/"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/ocaml
        dev-lang/perl:*
        sys-devel/bison
        sys-devel/flex
"

CMAKE_SOURCE="${WORKBASE}"/${PN}-${MY_PV}/src

elkhound_src_install() {
    # No install target :(
    dobin ast/astgen elkhound/elkhound
    dolib ast/libast.a elkhound/libelkhound.a smbase/libsmbase.a
    dodoc "${CMAKE_SOURCE}"/elkhound/{license.txt,parsgen.txt,index.html,manual.html}
}

